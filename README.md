#Scrolling Text Overflow#

This simple code snippet allows for text to overflow the container without breaking out on to more than one line. The user can hover on the text to make it scroll.

##Quick Guide##

You will need to give your Title (the text you want to overflow) the following CSS style;

```
#!CSS

h1#overflow {
  white-space: nowrap;
  overflow:hidden;
  width: 100%;
}
```
[Optional] I would recommend giving the overflowing text an indication to the user that there is more text, with an Ellipsis (...). You can do this via CSS's text-overflow attribute.

```
#!CSS
.ellipsis {
  text-overflow: ellipsis;
}
```

Once you have the CSS in place, you will need the following jQuery code to make it function, where *#container* resembles the article/div the Title resides in.


```
#!jQuery

$("#container").mouseenter(function() {
	var __speed = 10; //lower number = faster scrolling
  var __scroller = $(this).children('h1#overflow');
	__scroller.removeClass("ellipsis"); //Optional
	var maxscroll = __scroller.outerWidth()+1;
	var speed = maxscroll * __speed;
	__scroller.animate({
		scrollLeft: maxscroll
	}, speed, "linear");
});
$("#container").mouseleave(function() {
	var __scroller = $(this).children('h1#overflow');        
	__scroller.stop();
	__scroller.addClass("ellipsis"); //Optional
	__scroller.animate({
		scrollLeft: 0
	}, 'fast');
});

```


###Limitations###

Currently not supported for mobile devices, as *:hover* isn't available. Potential idea would be to disable the script, and give the Title an overflow-x of auto, so the user can click-and-drag to see more of the title.